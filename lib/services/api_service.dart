import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response response = await dio.get(dio.options.baseUrl);
      return MovieModel(results: MovieModel.fromJson(response.data).results);
    } catch (e) {
      return null;
    }
  }
}
