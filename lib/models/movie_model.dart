class MovieModel {
  List<Results> results;

  MovieModel({this.results});

  MovieModel.fromJson(Map<String, dynamic> json) {
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  String originalTitle;
  String posterPath;
  bool video;
  double voteAverage;
  String title;
  int voteCount;
  int id;
  String releaseDate;
  bool adult;
  String overview;
  List<int> genreIds;
  String backdropPath;
  String originalLanguage;
  double popularity;
  String mediaType;

  Results(
      {this.originalTitle,
      this.posterPath,
      this.video,
      this.voteAverage,
      this.title,
      this.voteCount,
      this.id,
      this.releaseDate,
      this.adult,
      this.overview,
      this.genreIds,
      this.backdropPath,
      this.originalLanguage,
      this.popularity,
      this.mediaType});

  Results.fromJson(Map<String, dynamic> json) {
    originalTitle = json['original_title'];
    posterPath = json['poster_path'];
    video = json['video'];
    voteAverage = json['vote_average'];
    title = json['title'];
    voteCount = json['vote_count'];
    id = json['id'];
    releaseDate = json['release_date'];
    adult = json['adult'];
    overview = json['overview'];
    genreIds = json['genre_ids'].cast<int>();
    backdropPath = json['backdrop_path'];
    originalLanguage = json['original_language'];
    popularity = json['popularity'];
    mediaType = json['media_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['original_title'] = this.originalTitle;
    data['poster_path'] = this.posterPath;
    data['video'] = this.video;
    data['vote_average'] = this.voteAverage;
    data['title'] = this.title;
    data['vote_count'] = this.voteCount;
    data['id'] = this.id;
    data['release_date'] = this.releaseDate;
    data['adult'] = this.adult;
    data['overview'] = this.overview;
    data['genre_ids'] = this.genreIds;
    data['backdrop_path'] = this.backdropPath;
    data['original_language'] = this.originalLanguage;
    data['popularity'] = this.popularity;
    data['media_type'] = this.mediaType;
    return data;
  }
}
