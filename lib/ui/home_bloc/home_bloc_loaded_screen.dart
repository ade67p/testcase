import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => DetailPage(
                        argument: data[index],
                        data: data,
                      )));
            },
            child: movieItemWidget(data[index]),
          );
        },
      ),
    );
  }

  Widget movieItemWidget(Results data) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        child: Column(
          children: [
            Hero(
              tag: data.posterPath,
              child: Image.network(
                  "https://image.tmdb.org/t/p/w500/" + data.posterPath),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 25, horizontal: 20),
              child: (data.title == null)
                  ? Text("-")
                  : Text(data.title, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }
}
