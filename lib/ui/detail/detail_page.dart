import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

class DetailPage extends StatelessWidget {
  final List<Results> data;
  final Results argument;

  DetailPage({Key key, this.data, this.argument}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Detail Movie",
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Hero(
              tag: argument.posterPath,
              child: Image.network(
                  "https://image.tmdb.org/t/p/w500/" + argument.posterPath),
            ),
            Padding(
              padding: EdgeInsets.all(25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                        text: 'Judul: ',
                        style: TextStyle(color: Colors.black45),
                        children: [
                          TextSpan(
                            text:
                                (argument.title == null) ? "-" : argument.title,
                          ),
                        ]),
                  ),
                  _spacer(),
                  RichText(
                    text: TextSpan(
                        text: 'Tanggal Rilis: ',
                        style: TextStyle(color: Colors.black45),
                        children: [
                          TextSpan(
                            text: (argument.releaseDate == null)
                                ? "-"
                                : argument.releaseDate,
                          ),
                        ]),
                  ),
                  _spacer(),
                  RichText(
                    text: TextSpan(
                        text: 'Rating: ',
                        style: TextStyle(color: Colors.black45),
                        children: [
                          TextSpan(
                            text: (argument.voteAverage == null)
                                ? "-"
                                : argument.voteAverage.toString(),
                          ),
                        ]),
                  ),
                  _spacer(),
                  RichText(
                    text: TextSpan(
                        text: 'Overview: ',
                        style: TextStyle(color: Colors.black45),
                        children: [
                          TextSpan(
                            text: (argument.overview == null)
                                ? "-"
                                : argument.overview,
                          ),
                        ]),
                  ),
                  _spacer(),
                  RichText(
                    text: TextSpan(
                        text: 'Popularity: ',
                        style: TextStyle(color: Colors.black45),
                        children: [
                          TextSpan(
                            text: (argument.popularity == null)
                                ? "-"
                                : argument.popularity.toString(),
                          ),
                        ]),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _spacer() {
    return SizedBox(height: 10);
  }
}
